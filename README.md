# VSCode DevContainer Test

Use to verify that all requirements are installed to allow VSCode DevContainers to work correctly.

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

To install, follow [these instructions](https://gitlab.com/worcester/cs/kwurst/software-development-supplemental-materials/-/blob/main/Installing-Git-Docker-VSCode.md).

## Working in Visual Studio Code

From the GitLab page for this project, do the following:

1. Be sure you have docker running.
2. Click on the `Clone` button.

    ![Clone button](images/clone.png)
3. Choose `Open in your IDE` - `Visual Studio Code`. (Use `SSH` or `HTTPS` depending on whether you have an SSH key or not.) ![Open in IDE](images/openinide.png)
4. You will be asked if you want to reopen in a development container ![Reopen Dialog](images/DevContainer.png)
5. When it has finished building/loading, you will see `Dev Container: Docker in Docker` at the bottom left of the window ![Java Projects window](images/DevContainerRunning.png)

**If you got an error between steps 4 & 5, then something is not installed correctly. Be sure to record the error message, and ask for help.**

## Testing PlantUML Previewer

1. Open `README.md`
2. Press `CTRL+K V`
3. You may see a box in the preview window that says `Some content has been disabled in this document`
    - Click on it
    - Choose the option `Allow insecure local content`
4. Verify that the diagram below renders correctly:

```plantuml
@startuml

start

if (Can you see this diagram?) then (yes)
  :Docker, VSCode, Remote Extension are installed correctly
  PlantUML previewer is working in devcontainer;
  :You are done!;
else (no)
  :PlantUML previewer is not working correctly;
  :Ask for help;
endif

stop

@enduml
```
Copyright © 2022 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.